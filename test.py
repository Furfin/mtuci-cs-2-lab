
def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    my_word = my_word.replace(' ','')
  
    if len(my_word) == len(other_word):    
      return ''.join([other_word[i] for i in range(len(my_word)) if (my_word[i] == '_' and other_word[i] not in my_word) or my_word[i] == other_word[i]]) == other_word
    else:
      return False
print(match_with_gaps('ab__a','aboba'))