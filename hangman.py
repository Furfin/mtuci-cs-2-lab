# Problem Set 2, hangman.py
# Name:
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist


def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------


# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    return list(filter(lambda x: x not in letters_guessed, secret_word)) == []


def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''

    return ''.join([''.join([sim if sim in letters_guessed else '_ ']) for sim in secret_word])


def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    return ''.join(letter for letter in string.ascii_lowercase if letter not in letters_guessed)


def hangman(secret_word, letters_guessed=[], guesses=8, warnings=4):

    print('Please, enter only one letter at a time', '\n')
    print("Use only alphabets's letters", '\n')

    while True:

        if is_word_guessed(secret_word, letters_guessed):
            print(
                f'You won ! ({get_guessed_word(secret_word,letters_guessed)})')
            print(f"Your score is {guesses * len(''.join(set(secret_word)))}")
            break

        print(get_guessed_word(secret_word, letters_guessed))
        print(f'{guesses} guesses left !')
        print('Available letters:', get_available_letters(letters_guessed), '\n')

        letter = input('Enter letter: ')
        while letter not in get_available_letters(letters_guessed) and warnings != 1:
            if letter.isalpha():
                warnings -= 1
                letter = input(
                    f'({warnings} warnings left) Reenter your letter, that one was already used: ')
            else:
                warnings -= 1
                letter = input(
                    f'({warnings} warnings left) Reenter your letter, that one was incorrect: ')
        if warnings == 1:
            print('Too many warnings occurred, you were expelled')
            print(secret_word)
            break

        if letter not in secret_word:
            print('Wrong letter !')
            if letter in ['a', 'e', 'i', 'o', 'u']:
                guesses -= 2
            else:
                guesses -= 1
            if guesses == 0:
                print('You lose !')
                print(secret_word)
                break
        else:
            print('Right letter')
        letters_guessed.append(letter)

        print('---------------------------------------')


# -----------------------------------


def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    my_word = my_word.replace(' ', '')

    if len(my_word) == len(other_word):
        return ''.join([other_word[i] for i in range(len(my_word)) if (my_word[i] == '_' and other_word[i] not in my_word) or my_word[i] == other_word[i]]) == other_word
    else:
        return False


def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''

    for word in wordlist:
        if match_with_gaps(my_word, word):
            print(word, end=' ')
    print('\n')
    return None


def hangman_with_hints(secret_word, letters_guessed=[], guesses=8, warnings=4):

    print('Please, enter only one letter at a time', '\n')
    print("Use only alphabets's letters", '\n')

    while True:

        if is_word_guessed(secret_word, letters_guessed):
            print(
                f'You won ! ({get_guessed_word(secret_word,letters_guessed)})')
            print(f"Your score is {guesses * len(''.join(set(secret_word)))}")
            break

        print(get_guessed_word(secret_word, letters_guessed))
        print(f'{guesses} guesses left !')
        print('Available letters:', get_available_letters(letters_guessed), '\n')

        letter = input('Enter letter or "*" for a hint: ')
        while letter not in get_available_letters(letters_guessed) and warnings != 1:
            if letter.isalpha():
                warnings -= 1
                letter = input(
                    f'({warnings} warnings left) Reenter your letter, that one was already used: ')
            elif letter == '*':
                if len(set(get_guessed_word(secret_word, letters_guessed))) >= 4:
                    show_possible_matches(get_guessed_word(
                        secret_word, letters_guessed))
                else:
                    print('You have guessed not enough letters !')
                letter = input('Enter letter: ')
            else:
                warnings -= 1
                letter = input(
                    f'({warnings} warnings left) Reenter your letter, that one was incorrect: ')

        if warnings == 1:
            print('Too many warnings occurred, you were expelled')
            print(secret_word)
            break

        if letter not in secret_word:
            print('Wrong letter !')
            if letter in ['a', 'e', 'i', 'o', 'u']:
                guesses -= 2
            else:
                guesses -= 1
            if guesses == 0:
                print('You lose !')
                print(secret_word)
                break
        else:
            print('Right letter')
        letters_guessed.append(letter)

        print('---------------------------------------')


# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.
if __name__ == "__main__":
    print('Welcome to the game Hagman')
    secret_word = choose_word(wordlist)
    print(f'I am thinking of a word that is {len(secret_word)} letters long')
    print('---------------------------------------')
    hangman_with_hints(secret_word)

###############

    # To test part 3 re-comment out the above lines and
    # uncomment the following two lines.

    #secret_word = choose_word(wordlist)
    # hangman_with_hints(secret_word)
